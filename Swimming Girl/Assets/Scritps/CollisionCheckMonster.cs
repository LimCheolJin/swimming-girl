﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheckMonster : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            other.GetComponent<PlayerMovement>().CrySound.Play();
            GameObject.Find("Runner").GetComponent<PlayerMovement>().hp -= 10;
            Destroy(gameObject);
            
        }
    }
}
