﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRoad : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] Roads;
    public GameObject Road;
    public float time;
    private float Zpos = 30.0f;
    private int RoadCount = 0;
    private int patternNum = 0;
    public int MaxRange;
    void Start()
    {
        StartCoroutine(Repeat());
    }

    private IEnumerator Repeat()
    {
        while(true)
        {
            if (RoadCount < 20)
                time = 0.1f;
            else
                time = 3.0f;

            yield return new WaitForSeconds(time);

            Spawn();
        }
    }

        // Update is called once per frame
    void Spawn()
    {
        patternNum = Random.Range(0, MaxRange);
        //Debug.Log(patternNum);
        Road = Roads[patternNum];
        Instantiate(Road, new Vector3(0, 0, Zpos), Quaternion.identity);
        Zpos += 10.0f;
        ++RoadCount;
    }
}
