﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PressureText : MonoBehaviour
{
    private float playerPressure;
    int Pressure;
    public Text Pressuret;
    private GameObject player;
    private void Start()
    {
        player = GameObject.Find("Runner");
    }
    void FixedUpdate()
    {

        playerPressure = player.transform.position.y;
        Pressuret.text = (" Pressure " + playerPressure.ToString());
    }
}
