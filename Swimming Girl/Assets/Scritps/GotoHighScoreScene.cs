﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GotoHighScoreScene : MonoBehaviour
{
    public AudioSource sound;
    public void HighScoreButton()
    {
        sound.Play();
        Invoke("GotoHighScore", 0.1f);
    }
    private void GotoHighScore()
    {
        SceneManager.LoadScene("HighScoreScene");
    }
}
