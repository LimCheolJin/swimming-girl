﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheckItem : MonoBehaviour
{
    public AudioSource sound;
    
    private void OnTriggerEnter(Collider other)
    {
       
        if (other.transform.tag == "Player")
        {
            other.GetComponent<PlayerMovement>().EattingSound.Play();
            //Debug.Log("삭제");
            GameObject.Find("Runner").GetComponent<PlayerMovement>().EatingNum++;
            Destroy(gameObject);
            
        }
    }
}
