﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class RestartButtonEvent : MonoBehaviour
{
    public AudioSource sound;
    public void Button()
    {
        sound.Play();
        Invoke("RestartGame", 0.1f);
    }
    private void RestartGame()
    {
        SceneManager.LoadScene("StartScene");
    }
}
