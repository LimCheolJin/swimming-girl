﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ComputingHighScore : MonoBehaviour
{
    private int highScore;
    public Text objText;
    void Start()
    {
        highScore = PlayerPrefs.GetInt("highScore");
    }

    void FixedUpdate()
    {
        if (highScore < PlayerPrefs.GetInt("EatingNum"))
        {
            highScore = PlayerPrefs.GetInt("EatingNum");
            PlayerPrefs.SetInt("highScore", highScore);
        }
        objText.text = "HIGH SCORE " + " " + highScore.ToString() + " ";
    }
}