﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 0.0f;
    public Animator anim;

    public int hp;
    public int EatingNum = 0;
    public bool IsMove;
    public bool IsJumping;
    public bool IsDead;
    public float JumpPower;
    public float JumpTime;
    public float PositionY;
    public int EattingCount;
    public Rigidbody rigidBody;
    public Slider hpbarSlider;
    public AudioSource CrySound;
    public AudioSource DeathSound;
    public AudioSource EattingSound;
    //private AudioSource deathSound;

    void Start()
    {
        EattingCount = 0;
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        PositionY = transform.position.y;
        hp = 100;
        anim.Play("Idle");
        speed = 5.0f;
        JumpPower = 1.1f;
        hpbarSlider = Slider.FindObjectOfType<Slider>();
        //deathSound = GameObject.Find("Death").GetComponent<AudioSource>();

        IsMove = false;
        IsJumping = false;
        IsDead = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            // 점프
            IsJumping = true;
            Debug.Log("jump Start");
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            IsJumping = false;
            Debug.Log("Jump end");
        }
        Jump();
        Move();
        HPCheck();
        hpbarSlider.value = hp;
    }
    private void HPCheck()
    {
        if (gameObject.transform.position.y > 30.0f)
            hp = 0;
        if (hp <= 0)
        {
            PlayerPrefs.SetInt("EatingNum", EatingNum);
            DeathSound.Play();
            IsDead = true;
            anim.Play("Die");
        }
    }

    private void Move()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
        anim.SetBool("Swim 02", true);
    }

    private void Jump()
    {
        if(IsJumping)
        {
            //PositionY = transform.position.y;

        }
        if(IsJumping)
        {
            rigidBody.AddForce(Vector3.up * JumpPower, ForceMode.VelocityChange);
            IsJumping = false;
        }
    }
}
