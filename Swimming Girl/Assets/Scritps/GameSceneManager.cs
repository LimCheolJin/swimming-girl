﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject player;
    void Start()
    {
        player = GameObject.Find("Runner");
    }

    void FixedUpdate()
    {
        if (player.GetComponent<PlayerMovement>().IsDead)
        {
            Invoke("GotoGameOverScene", 1.5f);
        }
    }

    void GotoGameOverScene()
    {
        SceneManager.LoadScene("GameOverScene");
        player.GetComponent<PlayerMovement>().IsDead = false;
    }
}
