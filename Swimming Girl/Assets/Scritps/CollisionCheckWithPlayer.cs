﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionCheckWithPlayer : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.transform.tag == "Player")
        {
            //Debug.Log("삭제");
            Destroy(gameObject);
        }
    }
}
