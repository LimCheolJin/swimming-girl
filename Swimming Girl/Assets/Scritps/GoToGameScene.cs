﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GoToGameScene : MonoBehaviour
{
    public AudioSource sound;
    public void StartButton()
    {
        sound.Play();
        Invoke("GotoGame", 0.1f);
    }
    private void GotoGame()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
