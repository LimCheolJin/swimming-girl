﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotoClose : MonoBehaviour
{
    public AudioSource sound;
    public void GotoCloseButton()
    {
        sound.Play();
        Invoke("GotoCloseFunc", 0.1f);
    }
    private void GotoCloseFunc()
    {
        Application.Quit();
    }
}
