﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameOverScoreText : MonoBehaviour
{
    private int playerScore;
    public Text objText;
    // Start is called before the first frame update
    private void Start()
    {
        playerScore = PlayerPrefs.GetInt("EatingNum");
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        objText.text = ("  Max Score " + playerScore.ToString());
    }
}
